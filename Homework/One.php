<?php


namespace Gamma\ITP\Homework;


class One
{
    public function isAnagram($someText,$otherText){
        if(strlen($someText) == strlen($otherText)){
            $someText = str_split($someText);
            $otherText = str_split($otherText);
            sort($someText);
            sort($otherText);

            for($x = 0; $x < count($someText); $x++) {
                if($someText[$x] != $otherText[$x]){
                    return false;
                }
            }

            return true;
        }
        return false;
    }
}