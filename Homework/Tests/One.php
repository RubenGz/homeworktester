<?php


namespace Gamma\ITP\Homework\Tests;


use Gamma\ITP\Api\Data\TestCaseInterfaceFactory;
use Gamma\ITP\Api\HomeworkTesterInterface;

class One
{
    const TEST_CASE_DATA = [
        [
            'function' => 'isAnagram',
            'arguments' => ['listen', 'silent'],
            'expected' => true
        ],
        [
            'function' => 'isAnagram',
            'arguments' => ['elvis', 'lives'],
            'expected' => true
        ],
        [
            'function' => 'isAnagram',
            'arguments' => ['motorsport', 'magento'],
            'expected' => false
        ],
        [
            'function' => 'isAnagram',
            'arguments' => ['backpack', 'ruthsack'],
            'expected' => false
        ],
        [
            'function' => 'isAnagram',
            'arguments' => ['aaa', 'aaaa'],
            'expected' => false
        ],
        [
            'function' => 'isAnagram',
            'arguments' => ['', ''],
            'expected' => true
        ]
    ];

    /**
     * @var HomeworkTesterInterface
     */
    protected $tester;

    /**
     * @var \Gamma\ITP\Homework\One
     */
    protected $homeworkOne;

    /**
     * @var TestCaseInterfaceFactory
     */
    protected $testCaseInterfaceFactory;

    public function __construct(
        HomeworkTesterInterface $homeworkTester,
        \Gamma\ITP\Homework\One $homeworkOne,
        TestCaseInterfaceFactory $testCaseInterfaceFactory
    )
    {
        $this ->tester = $homeworkTester;
        $this ->homeworkOne = $homeworkOne;
        $this ->testCaseInterfaceFactory = $testCaseInterfaceFactory;
    }

    public function test()
    {
        $testCases = array_map(function ($testCaseData) {
            $testCase = $this->testCaseInterfaceFactory->create();
            return $testCase->setFunctionName($testCaseData['function'])
                ->setExpected($testCaseData['expected'])
                ->setArguments($testCaseData['arguments']);
        }, self::TEST_CASE_DATA);

        $this->tester->run($this->homeworkOne, $testCases);
    }
}