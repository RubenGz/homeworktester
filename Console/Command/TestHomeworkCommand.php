<?php


namespace Gamma\ITP\Console\Command;


use Exception;
use Gamma\ITP\Homework\Tests\One;
use Magento\Framework\Console\Cli;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class TestHomeworkCommand extends Command
{

    /**
     * @var One
     */
    protected $oneTester;

    public function __construct(
        One $oneTester,
        ?string $name = null
    )
    {
        parent::__construct($name);

        $this->oneTester = $oneTester;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('itp:homework:test')
            ->setDescription('Runs tests on all the homework so far.');

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->oneTester->test();
        } catch (Exception $e) {
            $output->writeln("<error>Error testing homework: {$e->getMessage()}.</error>");
            return Cli::RETURN_FAILURE;
        }

        return Cli::RETURN_SUCCESS;
    }
}