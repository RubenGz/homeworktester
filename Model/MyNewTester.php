<?php


namespace Gamma\ITP\Model;

use Exception;
use Gamma\ITP\Api\Data\TestCaseInterface;
use Gamma\ITP\Model\HomeworkTester;


class MyNewTester extends HomeworkTester {
    /**
     * @Override
     * @param Object $homework
     * @param array $testCases
     * @return array
     *
     */
    public function run($homework, array $testCases): array
    {
        $mNow= microtime();
        $now = explode(" ",$mNow);
        $testCases = $this->runAllCases($homework, $testCases);
        $mThen= microtime();
        $then = explode(" ",$mThen);
        $this ->showSummary( $testCases, $then, $now, $mThen, $mNow);

        return $testCases;
    }


    /**
     * @Override
     * Runs a single test case on a given homework
     *
     * @param $homework
     * @param $testCase
     */
    protected function runTestCase($homework, TestCaseInterface &$testCase)
    {
        $functionName = $testCase->getFunctionName();
        $result = $homework->$functionName(...$testCase->getArguments());

        $testCase->setPassed($result === $testCase->getExpected())
            ->setResult($result);
        $args = implode(', ', $testCase->getArguments());
        echo "\n\nTest case: {$testCase->getFunctionName()}(". implode(', ', $testCase->getArguments()) . "): " .($testCase->getPassed() ? 'PASSED' : 'FAILED') . "\n";
        if(!$testCase->getPassed()){
            echo "     -------> Expected Output: " . ($testCase->getExpected() ? 'TRUE' : 'FALSE') . " and obtained " . ($result ? 'TRUE' : 'FALSE') . "\n";
        }
        //echo $perroIsunde;    <----- Generar excepciones
    }

    /**
     * @Override
     * Records an exception as the test case result, setting its passed to false
     *
     * @param TestCaseInterface $testCase
     * @param Exception $exception
     */
    protected function handleTestException(TestCaseInterface &$testCase, Exception $exception)
    {
        echo "     *****\n";
        $testCase->setPassed(false)
            ->setResult($exception);
        echo "     WARNING! -> {$exception->getMessage()}\n";
        echo "     *****\n";
    }

    /**
     *
     * Echoes the summary of the results
     * @param array $testCases
     * @param $then Date obtained after the tests were completed
     * @param $now Date obtained before the tests started to run
     * @param $mThen microtime obtained after the tests were completed
     * @param $mNow microtime obtained before the tests started to run
     */
    public function showSummary(array $testCases, $then, $now, $mThen, $mNow)
    {
        $passedTests=0;
        $failedTest=0;
        foreach ($testCases as $testCase) {
            $testCase->getPassed() ? $passedTests+=1 : $failedTest +=1;
        }
        $timeDiff = (($then[1]) - $now[1]) %86400 +((float)$mThen - (float)$mNow);
        echo "------------------------- Summary -------------------------\n";
        echo "\n" . $passedTests + $failedTest . " Test(s) completed in " . $timeDiff . " second(s)\n". "{$passedTests} Succesfull Test(s) and {$failedTest} Failed Test(s)\n";
    }
}
